

const projectsData = [ 
	{
		name: 'First Portfolio (HTML)',
		src: '/1st.png',
		link: 'https://jmpaed.gitlab.io/csp1-portfolio-2/#landing'
	},
	{
		name: 'Covid Tracker App',
		src: '/covidtracker.png',
		link: 'https://covid-tracker-smoky-nine.vercel.app/'
	},
	{
		name: 'Course Booking App',
		src: '/course_booking.jpg',
		link: 'https://jmpaed.gitlab.io/capstone2_frontend/'

	},
	{
		name: 'Budget Tracker App',
		src: '/budget_tracker.png',
		link: 'https://budget-tracker-vercel.vercel.app/'
	}


]

export default projectsData