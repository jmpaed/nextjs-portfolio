

const skillsData = [ 
	{
		name: 'HTML',
		src: '/html.png'
	},
	{
		name: 'CSS',
		src: '/css.png'
	},
	{
		name: 'Javascript',
		src: '/js.png'
	},
	{
		name: 'MongoDB',
		src: 'mongo.png'
	},
	{
		name: 'ExpressJS',
		src: '/express.png'
	},
	{
		name: 'ReactJS',
		src: '/react.png'
	},
	{
		name: 'NodeJS',
		src: '/node.png'
	},
	{
		name: 'NextJS',
		src: '/next.png'
	},
	{
		name: 'Git',
		src: '/git.png'
	},


]

export default skillsData