import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Banner from '../components/Banner.js';
import About from './About.js';
import Contact from './Contact.js'
import Image from 'next/image'

export default function Home() {
  return (
    <>

    
      <Head>
        <title>JM Paed</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Banner />
      <About />
      <Contact />

      <div>
      <footer className={styles.footer}>
              <strong><p ><a href="https://www.linkedin.com/in/jmpaed07/" target="_blank" >Jan Paed</a></p></strong>  
        </footer>
      </div>

   
    </>
  )
}
