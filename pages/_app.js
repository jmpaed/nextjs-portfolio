import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
