import {Row, Jumbotron, Container} from 'react-bootstrap';
import skillsData from '../data/skills.js'
import Skills from '../components/_skillz.js'
import styles from '../styles/Home.module.css';
import Projects from '../components/_proj.js'
import projectsData from '../data/projects.js'

export default function About(){


	const mySkills = skillsData.map(skills => {

		return(
			<Skills key={skills} skills={skills}/>
			)
	})

	const myProjects = projectsData.map(project => {

		return(
			<Projects key={project} projects={project}/>
			)
	})

	return(
		<>
		<Jumbotron id="AboutJumbo">
		<main className={styles.main} id="About">
        <h1 className={styles.title}>
          About me
        </h1>
        <Container className="my-5 text-center justify-content-center">
        <h5>
          I am an enthusiastic marketing student on my final year, and I'm also a fresh graduate of Zuitt in Full-Stack Web Development. I am eager to contribute to team success through hard work, attention to detail and excellent organizational skills. I am a fast learner and an avid admirer of knowledge. Motivated to learn, grow and excel in Marketing & E-commerce Industry, as well as the Web Development Industry. 
        </h5>
		<h1 className="mx-auto my-5 " >My Skills</h1>
		<Row className="justify-content-center" id="Skillgrid">
			<div className={styles.grid}>
				{mySkills}
			</div>
		</Row>
		<h1 className="mx-auto my-5" >My Projects</h1>
		<Row className="justify-content-center" id="Skillgrid">
			<div className={styles.grid}>
				{myProjects}
			</div>
		</Row>
        </Container>
		</main>
		</Jumbotron>

		
		</>
		)
}