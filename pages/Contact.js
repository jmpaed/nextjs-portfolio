import {Form} from 'react-bootstrap';
import {Row, Jumbotron, Container, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '../styles/Home.module.css';

export default function Contact() {
	return (
		<>
		<Container id="Contact" className="mx-auto my-5 text-center">
		<Jumbotron className="mx-auto" id="Contact">
		<h1 className={styles.title}>Work with me!</h1>
    <br />
    <br />

    <p className="text-center" style={{fontSize: 30}}><a href="https://mail.google.com/mail/u/0/?fs=1&to=jmpaed@gmail.com&tf=cm" target="_blank"><strong>Send me an email!</strong></a></p>
	
    <div className="map-responsive ">

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.4461420151647!2d121.0468731152136!3d14.573635589819622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c8443f0dd6d1%3A0xe4e4e27d51d17e84!2sSM%20Light%20Residences!5e0!3m2!1sen!2sph!4v1608044377307!5m2!1sen!2sph" width="505" height="500" frameborder="1" allowfullscreen="" aria-hidden="false" tabindex="0" />
          <br />
          <br />
          <p>SM Light Residences, Mandaluyong, 1550</p>

        </div>

	</Jumbotron>
	</Container>
</>
	)
}