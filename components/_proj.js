import {Card, Col, Button} from 'react-bootstrap';

export default function Projects({projects}){


	return(
		<>
			<Col>
				<Card style={{ width: '50rem', backgroundColor: 'transparent', opacity: '.8' }} className="m-2 text-center" border="white">
				  <Card.Body>
				    <Card.Title>{projects.name}</Card.Title>
				    <Card.Img variant= "top" src={projects.src}/>
				    <br />
				    <br />
				    <Button variant="secondary" href={projects.link} target="_blank">Hosted Link</Button>
				  </Card.Body>
				</Card>
			</Col>
		</>
		)
}