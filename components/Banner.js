import {Jumbotron, Button, Card, Row, Col, Container} from 'react-bootstrap';
import styles from '../styles/Home.module.css';



export default function Banner() {

  return (
    <>
    <main className={styles.main} id="Splash">
    <div id="BgV"> 
    <video className='videoTag' autoPlay loop muted>
    <source src="BgVid.mp4" type='video/mp4' />
    </video>
    </div>
    <div id="inner">
        <h1 className={styles.title} id="fswd">
          Jan Michael Paed
        </h1>
        <Container className="my-5 text-center justify-content-center">
        <h5>
          Full-Stack Web Developer
        </h5>
        </Container>
    </div>
    </main>
    </>
  )
}