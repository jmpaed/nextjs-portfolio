import {Card, Col} from 'react-bootstrap';

export default function Skills({skills}){

	return(
		<>
			<Col>
				<Card style={{ width: '13.5rem', backgroundColor: 'transparent', opacity: '.8' }} className="m-2 mr-auto " border="white">
				  <Card.Body>
				    <Card.Title>{skills.name}</Card.Title>
				    <Card.Img variant= "right" src={skills.src} />
				  </Card.Body>
				</Card>
			</Col>
		</>
		)
}